module.exports = {
    paths: {
        scripts: {
            src:  './src/scripts/',
            dest: './assets/js/',
            name: 'scripts.js'
        },
        styles: {
            src:  './src/styles/',
            dest: './assets/css/',
            name: 'styles.css'
        },
        images: {
            directory: './assets/images/'
        }
    },
    options: {
        jsCompiler: 'babel',
        jsExtension: 'js',
        useSourcemaps: true
    }
};