const
    args = require('yargs').argv
;

module.exports = function() {
    return args.hasOwnProperty('env') && args.env == 'production';
};