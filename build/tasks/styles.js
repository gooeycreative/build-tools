const
    gulp        = require('gulp-help')(require('gulp')),
    config      = require('../config'),
    sass        = require('gulp-sass'),
    rename      = require('gulp-rename'),
    args        = require('yargs').argv,
    forProd     = require('../for-production')()
;

module.exports = function() {
    let compilerOptions = {
        outputStyle: forProd ? 'compressed' : 'expanded',
        sourceComments: !forProd
    };

    gulp.src(config.paths.styles.src + '**/*.scss')
        .pipe(sass(compilerOptions))
        .pipe(rename(config.paths.styles.name))
        .pipe(gulp.dest(config.paths.styles.dest));
};