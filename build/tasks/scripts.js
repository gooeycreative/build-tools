const
    gulp        = require('gulp-help')(require('gulp')),
    config      = require('../config'),
    concat      = require('gulp-concat'),
    clone       = require('gulp-clone'),
    compilers   = {
        babel: require('gulp-babel'),
        ts: require('gulp-typescript')
    }
;

module.exports = function() {
    let compilerName = config.options.jsCompiler;
    let compiler = compilers[compilerName];
    let stream = gulp.src(config.paths.scripts.src + '**/*.' + config.options.jsExtension);

    switch (compilerName) {
        case 'babel' :
            stream.pipe(compiler({
                presets: ['es2015']
            }));
        break;

        case 'ts' :
            stream.pipe(compiler({
                removeComments: true,
                allowJs: true,
                target: 'ES5'
            }));
        break;

        default:
            //@TODO implement error reporting for non-existant compiler
        break;
    }

    stream.pipe(concat(config.paths.scripts.name))
        .pipe(gulp.dest(config.paths.scripts.dest));
};