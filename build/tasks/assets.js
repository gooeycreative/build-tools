const
    gulp        = require('gulp-help')(require('gulp')),
    config      = require('../config'),
    svg2png     = require('gulp-svg2png'),
    image       = require('gulp-image'),
    forProd     = require('../for-production')()
;

module.exports = function() {
    gulp.src(config.paths.images.directory + '**/*.svg')
        .pipe(svg2png())
        .pipe(gulp.dest(config.paths.images.directory));
    
    if (forProd) {
        gulp.src(config.paths.images.directory + '*')
            .pipe(image())
            .pipe(gulp.dest(config.paths.images.directory));
    }
};