# Gooey Build Tools

These are the build tools that we use for our internal projects, 
utilising a selection of packages to achieve multiple different 
tasks with less effort:

- SASS/SCSS compilation
- JavaScript/TypeScript compilation
- Source minification/compression
- Image compression and conversion
- Theme deployment
- Build testing

The majority of projects we build are based on the WordPress 
platform, so our internal tools are fairly centralised around 
that platform, but the source can just as easily be used in any 
other environment with a few minor tweaks.

## Installation

Installation requires very minimal effort. The only files and 
directories that you need from this repository are:

- `package.json`
- `Gulpfile.js`
- `build/`

So either clone the repository, remove the `.git` folder and any 
files that you don't need, or just download the files directly 
from the source.

In a WordPress environment, these files should go in your theme.

In the `build/` directory, there is also a file called `config.template.js`. You will need to rename this to `config.js`.

## Usage

All build tool commands should be run into the directory where the build tools have been installed.

### For the first time

When you've copied the build tools into your project, there 
won't (shouldn't) be any instance(s) of the packages required 
included. To install these, run the following command:

```
npm install
```

Once this has completed, you should be able to run the build tools.

### Building your source files

Assuming you've configured the build tools as per the section below, you can compile your build with the following command:

```
gulp build
```

This will run all tasks that have been registered.

### Running individual tasks

Individual tasks can be run with the usual build command, except appended with `:{task-name}`. For example, to run the "scripts" task to compile all of your scripts, you'd run the following:

```
gulp build:scripts
```

### Watching files

Watching files is ran almost identically to the `build` command, except you use `watch` instead:

```
gulp watch          # watch everything
gulp watch:scripts  # only watch scripts
```

## Configuration

If you haven't already, you'll need to rename `config.template.js` in the `build/` directory.

There are a series of options available in here which you can read about below:

| Name | Type | Description |
|------|------|-------------|
| options.jsCompiler | string | The compiler that you want to use ("babel" or "ts") |
| options.jsExtension | string | The extension that the compiler should look for |
| options.useSourcemaps | boolean | Whether to use sourcemaps |
| path.scripts.src | string | The directory where your script source files are |
| path.scripts.dest | string | The destination directory for the compiled script(s) |
| path.scripts.name | string | The filename for the compiled script(s) |
| path.styles.src | string | The directory where your source styles are |
| path.styles.dest | string | The destination directory for the compiled styles |
| path.styles.name | string | The filename for the compiled styles |
| path.images.directory | string | The directory where your build images are stored |

## Licensing

The software in this repository is licensed under the MIT 
software license.

```
Copyright (c) 2017 Gooey Creative

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```