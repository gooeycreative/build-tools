const
    gulp    = require('gulp-help')(require('gulp')),
    config  = require('./build/config'),
    args    = require('yargs').argv

    // compilation tasks
    buildScripts    = require('./build/tasks/scripts'),
    buildStyles     = require('./build/tasks/styles'),
    buildAssets     = require('./build/tasks/assets')
;

/**
 * Tasks
 */

//@TODO define a helper for the default "gulp" command

// build tasks
gulp.task('build:scripts', 'Compile all scripts from source', buildScripts);
gulp.task('build:styles', 'Compile all styles from source', buildStyles);
gulp.task('build:assets', 'Run optimisation on images', buildAssets);

// watch tasks
gulp.task('watch:scripts', function() {
    gulp.watch(config.paths.scripts.src + '**/*.js', ['build:scripts']);
});

gulp.task('watch:styles', function() {
    gulp.watch(config.paths.styles.src + '**/*.scss', ['build:styles']);
});

// full tasks
gulp.task('build', 'Compile all project sources', ['build:scripts', 'build:styles']);
gulp.task('watch', 'Watch project for changes', ['watch:scripts', 'watch:styles']);